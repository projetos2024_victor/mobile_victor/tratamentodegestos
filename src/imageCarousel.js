import React, { useState } from "react";
import { View, Image, StyleSheet, PanResponder, Animated } from "react-native";

const ImageCarousel = () => {
  const images = [
    "https://www.cnnbrasil.com.br/wp-content/uploads/sites/12/2024/02/TN220240225080-1-e1709019517846.jpg?w=1024",
    "https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Equals_sign_in_mathematics.jpg/220px-Equals_sign_in_mathematics.jpg",
    "https://i0.wp.com/paulogala.com.br/wp-content/uploads/2019/01/img_0332.jpg?fit=737%2C416&ssl=1",
  ];
  const [index, setIndex] = useState(0);
  const pan = useState(new Animated.ValueXY())[0];

  const panResponder = PanResponder.create({
    onStartShouldSetPanResponder: () => true,
    onPanResponderMove: Animated.event([
      null,
      { dx: pan.x, dy: pan.y },
    ]),
    onPanResponderRelease: (e, gestureState) => {
      if (gestureState.dx > 50) {
        setIndex((prevIndex) => (prevIndex - 1 + images.length) % images.length);
      } else if (gestureState.dx < -50) {
        setIndex((prevIndex) => (prevIndex + 1) % images.length);
      }
      Animated.spring(pan, { toValue: { x: 0, y: 0 } }).start();
    },
  });

  return (
    <View style={styles.container}>
      <Animated.View
        style={[
          styles.imageContainer,
          {
            transform: [{ translateX: pan.x }],
          },
        ]}
        {...panResponder.panHandlers}
      >
        <Image
          source={{ uri: images[index] }}
          style={styles.image}
        />
      </Animated.View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#f0f0f0",
  },
  imageContainer: {
    width: 200,
    height: 200,
    alignItems: "center",
    justifyContent: "center",
  },
  image: {
    width: "100%",
    height: "100%",
    borderRadius: 10,
  },
});

export default ImageCarousel;
