// HomeScreen.js
import React from "react";
import { View, Button, StyleSheet } from "react-native";

export default function HomeScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <Button
        title="Count Moviment"
        onPress={() => navigation.navigate("CountMoviment")}
      />
      <Button
        title="Image Moviment"
        onPress={() => navigation.navigate("ImageMoviment")}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
