import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import countMoviment from "./src/CountMoviment";
import imageMoviment from "./src/imageCarousel";
import HomeScreen from "./src/HomeScreen";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="HomeScreen">
        <Stack.Screen name="HomeScreen" component={HomeScreen} />
        <Stack.Screen name="CountMoviment" component= {countMoviment} />
        <Stack.Screen name="ImageMoviment" component= {imageMoviment} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}